# BB Apoio - Administração #

Esse projeto é referente ao Challenge FIAP 2020 para a BB Seg

### Sobre o Projeto ###

* Primeiro Protótipo Web
* Para a administração
* HTML, CSS e Javascript
* Apenas Front-End

### Grupo AEGIS ###

* Nathália Kimura Yonezawa
* Lucas Yoshimitsu Torigoe Utida
* Gabriel Barbosa dos Santos
* Bárbara Nicole O. V. Alves
* Gustavo Velosa Macario